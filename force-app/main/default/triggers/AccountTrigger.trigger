trigger AccountTrigger on Account (before insert, after insert) {
    
    if(trigger.isInsert){
        if(trigger.isBefore){
            AccountController.popullateAddress(trigger.New, 'Before Trigger');           
            
        }
        if(trigger.isAfter){
            AccountController.popullateAddress(trigger.New, 'After Trigger');
        }
    }
    

}