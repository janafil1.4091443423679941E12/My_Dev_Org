trigger OrderEventTrigger on Order_Event__e (after insert) {
List<Task> lstTask = new List<Task>();

for(Order_Event__e event: Trigger.New) {
if(event.Has_Shipped__c == true) {
 Task ta=new Task();
 ta.Priority = 'Medium';
 ta.Status = 'New';
 ta.Subject = 'Follow up on shipped order ' + event.Order_Number__c;
 ta.OwnerId = event.CreatedById;
 
 lstTask.add(ta);

}

insert lstTask;

}


}