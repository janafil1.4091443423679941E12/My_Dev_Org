import { LightningElement, track, wire } from 'lwc';
import {CurrentPageReference} from 'lightning/navigation';
import {registerListener, unregisterAllListeners} from 'c/pubsub';

export default class SubComp extends LightningElement {
    @wire(CurrentPageReference) pageRef;
    @track valFromParent;
    connectedCallback(){
        registerListener('searchAccount', this.handleCallback, this);
        
    }
    
    disconnectedCallback(){
        unregisterAllListeners(this);
    }

    handleCallback(detail){
        this.valFromParent = detail.accountName;
        console.log('EventMsg:' + detail.accountName);
    }

}