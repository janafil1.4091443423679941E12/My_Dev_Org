import { LightningElement, track } from 'lwc';
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import NAME_FIELD from '@salesforce/schema/Account.Name';
import {NavigationMixin} from 'lightning/navigation'
import { encodeDefaultFieldValues } from 'lightning/pageReferenceUtils';
export default class createRecordLWC extends NavigationMixin(LightningElement) {
    @track accountId;
    name = '';

    handleNameChange(event) {
        this.accountId = undefined;
        this.name = event.target.value;
        console.log('$$$'+ this.name);
    }
    createAccount(){
        const fields = {};
        fields[NAME_FIELD.fieldApiName] = this.name;
        const recordInput ={ apiName: ACCOUNT_OBJECT.objectApiName,fields};
        createRecord(recordInput)
        .then(account => {
            this.accountId =account.id;

            const defaultValues = encodeDefaultFieldValues({
                FirstName: 'NTT',
                LastName: 'DATA',
                LeadSource: 'Web'
            })

            this[NavigationMixin.Navigate]({
                type : 'standard__objectPage',
                attributes : {
                    objectApiName : 'Contact',
                    actionName : 'new'
                },
                state : {
                    defaultFieldValues : defaultValues
                }
            })


            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Account Created',
                    varient: 'success',

                }),
            );
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error creating record',
                        message: error.body.message,
                        variant: 'error',
                }),
            );
        }

        )

    }

    createNewAccount(event){
        this[NavigationMixin.Navigate]({
            type : 'standard__objectPage',
            attributes : {
                objectApiName : 'Account',
                actionName : 'new'
            }
        })

    }
}