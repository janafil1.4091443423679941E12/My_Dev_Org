import { LightningElement, api, track,wire } from 'lwc';
import {CurrentPageReference} from 'lightning/navigation';
import {registerListener} from 'c/pubsub';
export default class ChildComp extends LightningElement {
    @api parentmsg;
    @api parentmsg1;
    @track msgToParent;
    @wire(CurrentPageReference) pageRef;


    handleClick(event) {
        this.msgToParent = this.template.querySelector('lightning-input').value;

        let cusEvent = new CustomEvent('notification',{detail :  this.msgToParent});
        this.dispatchEvent(cusEvent);
    }

    connectedCallback(){
        registerListener('searchAccount', this.handleCallBack, this);
    }

    handleCallBack(detail){
        this.msgToParent = detail.accountName;
    }

}