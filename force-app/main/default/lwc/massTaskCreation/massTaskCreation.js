import { LightningElement, track } from 'lwc';

export default class MassTaskCreation extends LightningElement {

    keyIndex = 0;
    @track itemList = [
        {
            id: 0
        }
    ];

    addRow() {
        ++this.keyIndex;
        var newItem = [{ id: this.keyIndex }];
        this.itemList = this.itemList.concat(newItem);
    }

    connectedCallback(){
        this.addRow();
    }

}