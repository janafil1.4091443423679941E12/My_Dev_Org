import { LightningElement, track, wire, api } from 'lwc';
import {CurrentPageReference} from 'lightning/navigation';
import {fireEvent} from 'c/pubsub';
export default class ParentComp extends LightningElement {

    @api fromAura;
    @api 
    invokeFromAura(){
        console.log('Called from Aura parent component');
    }
    raiseEvent(event){
        let txtInput = this.template.querySelector(".txtInput");
        const v = txtInput.value;
        const textChangeEvent = new CustomEvent('txtChange',{
            detail : {v},
        });

        //Fire Event
        this.dispatchEvent(textChangeEvent);
    }
    handleChange(event) {
        alert('text changed');
        console.log('handlermethod');
        let v = event.target.value;
        console.log('$$$txtValue:'+ txtValue);
        let valueChangeEvent = new CustomEvent("valueChange", {
            detail: { v }
          });

          this.dispatchEvent(valueChangeEvent);

    }


    @track msgFromParent ;
    @wire(CurrentPageReference) pageRef;
    handleClick(event) {
        console.log(this.template.querySelector('lightning-input').value);
        this.msgFromParent = this.template.querySelector('lightning-input').value;

    }
    handleChange(event){
        var eventParam = {'accountName' : event.target.value};
        fireEvent(this.pageRef,'searchAccount', eventParam);
        console.log('fired Event');
    }


    handleCallBack(event){
        this.msgFromParent = event.detail;
    }

}