import { LightningElement,api, track } from 'lwc';
import {FlowAttributeChangeEvent, FlowNavigationNextEvent} from 'lightning/flowSupport';

const column = [
    {lable : 'First Name', fieldName: 'FirstName'},
    {lable : 'Last Name', fieldName: 'LastName'},
]
export default class NewContact extends LightningElement {
    @api recordId;
    @api contactList;
    @api ContactRec;
    columns = column;
    //@track mapVal= new Map([[1 , "Saurabh"], [2 , "Supriya"] ,[3, "Yuvraj"]]);
    /*handleSubmit(event){
        event.preventDefault();
        const fields = event.detail.fields;
        fields.AccountId = this.recordId;
        this.template.querySelector('lightning-record-edit-form').submit(fields);
    }   

    handlerSuccess(event){
        const closeEvent = new CustomEvent('close',{detail: {mapVal1: this.mapVal}});
            // Dispatches the event.
            this.dispatchEvent(closeEvent);
    }

    handleClick(even){
        const closeEvent = new CustomEvent('close',{ detail:  {Fname : 'jana', Lname : 'sund'}});
            // Dispatches the event.
            this.dispatchEvent(closeEvent);
    } */
    NavigateToFlow(){
        this.ContactRec = this.contactList[0];
        const nextNavigationEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(nextNavigationEvent);


    }
}