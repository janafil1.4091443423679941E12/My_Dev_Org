/*import { LightningElement, track } from 'lwc';
export default class HelloWorld1 extends LightningElement {
    @track greeting = 'World';
    changeHandler(event) {
        this.greeting = event.target.value;
    }
} */

import { LightningElement, track } from 'lwc';
import searchAccounts from '@salesforce/apex/GetAllAccounts.searchAccount';

const columnList = [
    {label: 'Id', fieldName: 'Id'},
    {label: 'Name', fieldName: 'Name'},
    {label: 'Website', fieldName: 'Website'},
    {label: 'Industry', fieldName: 'Industry'}
];

export default class LightningDataTable extends LightningElement {
    @track accountList;
    @track columnList = columnList;
    @track noRecordsFound = true;

    findAccountResult(event) {
        const accName = event.target.value;

        if(accName) {
            searchAccounts ( {accName}) 
            .then(result => {
                this.accountList = result;
                this.noRecordsFound = false;
            })
        } else {
            this.accountList = undefined;
            this.noRecordsFound = true;
        }
    }

}