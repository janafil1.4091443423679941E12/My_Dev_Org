import { LightningElement , api , wire , track } from 'lwc';
import findUserListMd from '@salesforce/apex/myHBTUserAdminController.getPermissionSetAssignmentList';
const columns = [{ label: 'PermissionSet.Name', fieldName: 'PermissionSet.Name'},
{ label: 'Assignee.Username', fieldName: 'Assignee.Username' },
{type: "button", typeAttributes: {  
    label: 'Reset Password',  
    name: 'Reset Password',  
    title: 'Reset Password',  
    disabled: false,  
    value: 'Reset Password',  
    iconPosition: 'left'  
}}
];


export default class FindUserList extends LightningElement {
    @api emailaddr = 'janafil@gmail.com';
    @track userdata;
    message1 = "No data to display";
    columns = columns;
    @wire(findUserListMd, { emailadd: '$emailaddr' })
    userList({ error , data }){
        if (data){
               this.userdata = data.map( record => Object.assign(
                   {"PermissionSet.Name": record.PermissionSet.Name, "Assignee.Username": record.Assignee.Username}
               )) ; 
        }
        else if ( error ){

        }
    }
    //this.data = userList;

}