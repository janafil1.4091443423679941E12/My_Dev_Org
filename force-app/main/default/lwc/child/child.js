import { LightningElement } from 'lwc';

export default class Child extends LightningElement {

    renderedCallback(){
        throw new Execption('Error happend on Child');
    }
    disconnectedCallback(){
        console.log('From Child DisconnectedCallback');
    }

}