import { LightningElement, track, wire } from 'lwc';
import {CurrentPageReference} from 'lightning/navigation';
import {fireEvent} from 'c/pubsub';

export default class PubComp extends LightningElement {

    @track accName;
    @wire(CurrentPageReference) pageRef;

    handleChange(event){
        console.log(event.target.value);
        var eventParam = {'accountName' : event.target.value};
        fireEvent(this.pageRef,'searchAccount', eventParam);
        console.log('fired Event');
    }
}