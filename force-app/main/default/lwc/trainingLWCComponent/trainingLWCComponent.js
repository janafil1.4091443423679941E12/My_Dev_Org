import { LightningElement } from 'lwc';
import NAME_FIELD from '@salesforce/schema/Contact.Name';
import PHONE_FIELD from '@salesforce/schema/Contact.Phone';
export default class TrainingLWCComponent extends LightningElement {
    fields = [NAME_FIELD, PHONE_FIELD];


}