import { LightningElement, track, wire, api } from 'lwc';
import getOrgLicenses from '@salesforce/apex/OrgLiensesManagementHandler.getOrgLicenses'

export default class RemoteOrgLicenses extends LightningElement {

    @track orgLicenses;
    @track errorDetails;
    @api orgName;

    @wire(getOrgLicenses) 
    getRemoteOrg( { data, error } ) {
        if (error) {
            console.log('Error: '+ error);
            this.errorDetails = error;
        } else if (data) {
            console.log('Data: '+data);
            this.orgLicenses = data;
            console.log('$$$'+ this.orgLicenses.length);
        }
    }
    
}