import { LightningElement,api } from 'lwc';

export default class ParameterLWC extends LightningElement {
@api parameterFromOut;
@api contactRec;
@api contactRecs;
@api outText = 'From Lwc component';
}