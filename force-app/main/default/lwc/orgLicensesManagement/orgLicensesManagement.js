import { LightningElement, track, wire, api } from 'lwc';
import getOrgLicenses from '@salesforce/apex/OrgLiensesManagementHandler.getOrgLicenses'

import USER_LICENSES from '@salesforce/schema/UserLicense';
export default class OrgLicensesManagement extends LightningElement {

    @track orgLicenses;
    @track remoteOrgDetails = ["Org 1","Org 2","Org 3"];
    @track errorDetails;
    @track index =1;

   /*
    @wire(getOrgLicenses) 
    getOrgLic( { data, error } ) {
        if (error) {
            console.log('Error: '+ error);
            this.errorDetails = error;
        } else if (data) {
            console.log('Data: '+data);
            this.orgLicenses = data;
            console.log('$$$'+ this.orgLicenses.length);
        }
    } */

    @wire(getOrgLicenses) 
    getRemoteOrg( { data, error } ) {
        if (error) {
            console.log('Error: '+ error);
            this.errorDetails = error;
        } else if (data) {
            console.log('Data: '+data);
            this.orgLicenses = data;
            console.log('$$$'+ this.orgLicenses.length);
        }
    }

}