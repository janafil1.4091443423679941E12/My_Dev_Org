import { LightningElement, api } from 'lwc';
import template2 from './lifeCycleHook1.html';
import template3 from './lifeCycleHook.html';
export default class LifeCycleHook extends LightningElement {
Name = 'Test';
@api age = 10;
 hadRendered = true;
 temp2 = false;
 showChild = true;

    /*constructor() {
        super();

       console.log('From Constructor');

       console.log('Name' + this.Name);
       console.log('Age' + this.Age);

       this.Name = 'Test1';
        this.age = 30;

        console.log('Name: ' + this.Name);
       console.log('Age: ' + this.Age);

       let city = 'Chennai';
       console.log('Age: ' + city);
    } */

    /*connectedCallback(){
        console.log('Name' + this.Name);
        console.log('Age' + this.age);
 
        this.Name = 'Test1';
         this.age = 30;

         console.log('Name' + this.Name);
        console.log('Age' + this.age);

        console.log(this.template.querySelector('lightning-input'));

    } */

    handleButtonClick(event){
        //this.temp2 = true;
        
        if(this.showChild)
        this.showChild = false;
        else 
        this.showChild = true;
    }

    render(){

        if( this.temp2){
            return template2;
        }
        return template3;
    }

    renderedCallback(){
        console.log('From renderedCallback');
    }
    disconnectedCallback(){
        console.log('From disconnectedCallback');
    }
    
    errorCallback(error, stack){
        console.log('error:'+ error);
    }
}