import { api, LightningElement, track,wire } from 'lwc';
import getAccount from '@salesforce/apex/DemoController.getAccount';


export default class LdsDemo extends LightningElement {
    
    //Wire to property
    //@wire (getAccount, {searchkey: 'Orboid Bulk Company'}) accounts;

    //Wire to Method
    accRecords;
    accError;
    /*@wire (getAccount, {searchKey: 'LWC Demo Account'}) 
    getAcc(data, error){
        if(data){
           
            this.accRecords = data;
            console.log('$$$data:'+ JSON.stringify(this.accRecords));
        }
        else{
            this.accError = error;
            console.log('$$$Error:'+ JSON.stringify(this.accError));
        }
    }*/
    
    handleChange(event){
        console.log('$$$method:'+ event.target.value);
        getAccount({searchKey: event.target.value})
        .then(result => {
            console.log('resule:'+ JSON.stringify(result));
            this.accRecords = result;
        })
        .catch(errorData =>{
            this.accError = errorData;
        })

    }

}