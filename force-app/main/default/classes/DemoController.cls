public class DemoController {
    
    @AuraEnabled
    public static List<Account> getAccount(String searchKey){
        
        String filterText = '%'+ searchKey + '%';
        system.debug('$$$getAccount');
        return [SELECT Id, Name FROM Account WHERE Name LIKE :filterText];
    }

}