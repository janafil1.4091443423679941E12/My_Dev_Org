public class ToolingApi_callouts {

    public static void callToolAPI(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://ap15.salesforce.com/services/data/v45.0/tooling/sobjects');
		req.setMethod('GET');
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
        req.setHeader('Content-Type', 'application/json');
        Http h = new Http();
        HttpResponse res = h.send(req);
        Map<string,object> objMap=(Map<string,object>)JSON.deserializeUntyped(res.getBody());
        system.debug('$$$'+ objMap);
        
/*        
       ToolingAPI toolingAPI = new ToolingAPI();
        List<ToolingAPIWSDL.sObject_x> profileObject =    (List<ToolingAPIWSDL.sObject_x>)toolingAPI.query('Select Description, FullName, Metadata From Profile Where FullName = \'Admin\'').records;
System.debug('Description' + profileObject.Description);
System.debug('FullName' + profileObject.FullName);
System.debug('Metadata' + profileObject.Metadata);
System.debug('RT' + profileObject.Metadata.ProfileRecordTypeVisibility);
System.debug('VF' + profileObject.Metadata.ProfileApexPageAccess ); */
    }
}