public class ComponentListWrapper {
    public string componentName {get;set;}
    public string apiName{get;set;}
    public string componentType{get;set;}
    public string lastModifiedByName {get;set;}
    public datetime lastModifiedDate {get;set;}
    public Boolean isSelected {get;set;}
    private Map<String,String> cNames {get;set;}
    public ComponentListWrapper(string comName,string fullName,string lastModByName,datetime lastModDate){
        cNames =new Map<String,String>();
        cNames.put('profile', 'Profile');
        cNames.put('permissionset', 'PermissionSet');
        componentName =comName;
        apiName = fullName;
        componentType = cNames.get(fullName.substring(fullName.indexOf('.')+1,fullName.length()));
        lastModifiedByName = lastModByName;
        lastModifiedDate = lastModDate;
        
    }
}