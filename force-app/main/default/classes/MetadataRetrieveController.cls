public with sharing class MetadataRetrieveController 
{
    private static final Integer METADATA_API_VERSION = 
        Integer.valueOf(new MetadataService.MetadataPort().endpoint_x.substringAfterLast('/'));

    public List<SelectOption> MetaDataTypes {get; set;}
    public String MetaDataType {get; set;}      
    public List<SelectOption> MetaDataItems {get; set;}
    public String MetaDataFolder {get; set;} 
    public String MetaDataItem {get; set;}  
    public String MetadataFileName {get; set;}
    public String MetadataFileData {get; set;}
    public MetadataService.AsyncResult AsyncResult {get; private set;}
    public String MetaDataRetrieveZip { get; private set; } 
    public String MetaDataZip {get;set;}
    public List<MetadataFile> MetadataFiles { get; set; }       
    public List<ComponentListWrapper> lstCompWppr {get;set;}  
    private Map<String,string> componentsMap {get;set;}
    public static Map<string,string> mapMetadataType {get;set;}
    public string changeSetName {get;set;}
    public PageReference init()
    {
        // List available Metadata Types via the 'describeMetadata' API call
        MetadataService.MetadataPort service = createService();                 
        MetadataService.DescribeMetadataResult describeResult = service.describeMetadata(METADATA_API_VERSION);
        List<String> metadataTypeNames = new List<String>();
        mapMetadataType = new Map<string,string> ();
        for(MetadataService.DescribeMetadataObject metadataObject : describeResult.metadataObjects)
        {
            metadataTypeNames.add(metadataObject.xmlName);
            // Include child Metadata Types (such as CustomField, ValidationRule etc..)
            if(metadataObject.childXmlNames!=null)
                for(String childXmlName : metadataObject.childXmlNames){
                    if(childXmlName!=null){
                        metadataTypeNames.add(childXmlName);
                        
                    }
                        
                    
                }
                    
        }   

        // Sort Metadata Types
        metadataTypeNames.sort();
        MetaDataTypes = new List<SelectOption>();               
        for(String metadataTypeName : metadataTypeNames){
            mapMetadataType.put(metadataTypeName,metadataTypeName);
            MetaDataTypes.add(new SelectOption(metadataTypeName, metadataTypeName));
        }
            system.debug('$$$1mapMetadataType:'+mapMetadataType);
                    
        // Default to first Metadata Type returned  
        MetaDataType = MetaDataTypes[0].getValue();             
        // Retrieve Metadata items for the selected Metadata Type
        listMetadataItems();        
        return null;    
    }
    
    public PageReference listMetadataItems()
    {
        // List Metadata items for the selected Metadata Type
        MetaDataItems = new List<SelectOption>();       
        MetadataService.MetadataPort service = createService();             
        List<MetadataService.ListMetadataQuery> queries = new List<MetadataService.ListMetadataQuery>();        
        MetadataService.ListMetadataQuery queryLayout = new MetadataService.ListMetadataQuery();
        if(MetaDataFolder!=null && MetaDataFolder.length()>0)
            queryLayout.folder = MetaDataFolder;
        queryLayout.type_x ='PermissionSet'; //MetaDataType;
        queries.add(queryLayout);   
         queryLayout = new MetadataService.ListMetadataQuery();
        if(MetaDataFolder!=null && MetaDataFolder.length()>0)
            queryLayout.folder = MetaDataFolder;
        queryLayout.type_x ='Profile'; //MetaDataType;
         queries.add(queryLayout); 
        MetadataService.FileProperties[] fileProperties = service.listMetadata(queries, METADATA_API_VERSION);
        
        // Sort
        List<String> fullNames = new List<String>();
        if(fileProperties!=null)
        {
            lstCompWppr=new List<ComponentListWrapper>();
            for(MetadataService.FileProperties fileProperty : fileProperties){
                ComponentListWrapper compWpr=new ComponentListWrapper(fileProperty.fullName,fileProperty.fileName,
                                                                      fileProperty.lastModifiedByName,fileProperty.lastModifiedDate); 
                
                lstCompWppr.add(compWpr);
                    }
                //fullNames.add(fileProperty.fullName);
           // fullNames.sort();
           // for(String fullName : fullNames)
            //    MetaDataItems.add(new SelectOption(fullName,EncodingUtil.urlDecode(fullName, 'UTF-8')));
        }

        return null;    
    }
    
    public PageReference retrieveMetadataItem()
    {       
        componentsMap = new Map<string,string>();
        for(ComponentListWrapper clw:lstCompWppr){
            if(clw.isSelected){
                 if(componentsMap.containsKey(clw.componentType)){
                componentsMap.put(clw.componentType, componentsMap.get(clw.componentType) + ','+ clw.componentName);
            }
            else{
                componentsMap.put(clw.componentType, clw.componentName );
            }
            }
            
        }
        if(componentsMap.isEmpty()){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select any component to retrieve...'));
            return null;
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Retrieving metadata...'));
        
        // Reset state from any previous requests
        MetaDataRetrieveZip = null;
        MetadataFiles = null;
         
        // Construct unmanaged package with list of desired components to retrieve in zip
        MetadataService.MetadataPort service = createService();             
        MetadataService.RetrieveRequest retrieveRequest = new MetadataService.RetrieveRequest();
        retrieveRequest.apiVersion = METADATA_API_VERSION;
        retrieveRequest.packageNames = null;
        retrieveRequest.singlePackage = true;
        retrieveRequest.specificFiles = null;
        retrieveRequest.unpackaged = new MetadataService.Package_x();
        retrieveRequest.unpackaged.types = new List<MetadataService.PackageTypeMembers>();
        //MetadataService.PackageTypeMembers packageType = new MetadataService.PackageTypeMembers();
        //packageType.name = 'Profile'; 
        //packageType.members = new String[] { MetadataFolder, MetaDataItem };
            //packageType.members = new String[] { 'Admin' };
            //    retrieveRequest.unpackaged.types.add(packageType);
        MetadataService.PackageTypeMembers packageType = null;
        for(string comType:componentsMap.keySet()){
            system.debug('$$$comType:'+comType);
            packageType = new MetadataService.PackageTypeMembers();
            system.debug('$$$2mapMetadataType:'+mapMetadataType);
            packageType.name = comType;
            string[] metaItem=componentsMap.get(comType).split(',');
            
           // metaItem[0] =MetadataFolder;
            packageType.members =metaItem; //new String[] { MetadataFolder, 'Admin'};
            retrieveRequest.unpackaged.types.add(packageType);
         }       
        
        packageType = new MetadataService.PackageTypeMembers();
        packageType.name = 'RecordType'; 
        packageType.members = new String[] { MetadataFolder, '*' };
        retrieveRequest.unpackaged.types.add(packageType);

        AsyncResult = service.retrieve(retrieveRequest);
                        
        return null;    
    }
    
    public PageReference checkAsyncRequest()
    {
        // Check the status of the retrieve request
        MetadataService.MetadataPort service = createService();
        MetadataService.RetrieveResult retrieveResult = service.checkRetrieveStatus(AsyncResult.Id, true);
        if(retrieveResult.done)
        {
            // Errors?
            if(retrieveResult.status != 'Succeeded')
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, retrieveResult.errorMessage));
                AsyncResult = null;             
            }
            else
            {
                // Place Base64 encoded zip data onto the page for the JSZip library to handle
                MetaDataRetrieveZip = retrieveResult.zipFile;
                MetadataFiles = new List<MetadataFile>();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Expanding...'));
                AsyncResult = null;
            }
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Retrieving metadata...'));
        }   

        return null;
    }
    
    public PageReference receiveMetadataZipFile()
    {
        // In this example the retrieved metadata is stored in viewstate in production cases you would probably use a custom object / attachment
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Expanding ' + MetaDataFileName + '...'));
        MetadataFile metaDataFile = new MetadataFile();
        metaDataFile.fullName = MetaDataFileName;
        metaDataFile.content = MetaDataFileData;
        MetadataFiles.add(metaDataFile);
        return null;
    }
    public PageReference createChangeSet(){
        system.debug('$$$createChangeSet Called...');
        Changeset__c cs=new Changeset__c();
        cs.Name = changeSetName;
        insert cs;
        system.debug('$$$createChangeSet cs:'+ cs);
        List<Manifest__c> lstManifest = new  List<Manifest__c>();
        
        for(ComponentListWrapper clw:lstCompWppr){
            Manifest__c mf=new Manifest__c();
            if(clw.isSelected){
                mf.Changeset__c = cs.Id;
                mf.Name = clw.componentName;
                mf.Api_Name__c = clw.apiName;
                mf.Type__c=clw.componentType;
                lstManifest.add(mf);
            }
        }
        system.debug('$$$createChangeSet lstManifest:'+ lstManifest);
        if(lstManifest!=null && !lstManifest.isEmpty()){
            insert lstManifest;
        }
        deployZip();
        return null;
    }
    
    public PageReference receiveComplete()
    {
        
       List<Organizations__c> lstOrg=[select id from Organizations__c where Name='Jana-Dev'];
        system.debug('$$$createChangeSet Called...');
        Changeset__c cs=new Changeset__c();
        cs.Name = changeSetName;
        cs.Organization__c =lstOrg[0].id;
        insert cs;
        system.debug('$$$createChangeSet cs:'+ cs);
        List<Manifest__c> lstManifest = new  List<Manifest__c>();
        Attachment att=new Attachment();
        att.ParentId=cs.Id;
        att.Name = 'ZipFile';
        att.Body= Blob.valueOf(MetaDataRetrieveZip);
        insert att;
        for(ComponentListWrapper clw:lstCompWppr){
            Manifest__c mf=new Manifest__c();
            if(clw.isSelected){
                mf.Changeset__c = cs.Id;
                mf.Name = clw.componentName;
                mf.Api_Name__c = clw.apiName;
                mf.Type__c=clw.componentType;
                lstManifest.add(mf);
            }
        }
        system.debug('$$$createChangeSet lstManifest:'+ lstManifest);
        if(lstManifest!=null && !lstManifest.isEmpty()){
            insert lstManifest;
        }
         // Completed, cleared Base64 encoded zip data from viewstate
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Metadata retrieve completed'));
        deployZipfile(MetaDataRetrieveZip);
        MetaDataZip = MetaDataRetrieveZip;
        MetaDataRetrieveZip = null;     
        return null;
    }
    @future(callout=true)
    public static void deployZipfile(string zipfile){
       /*HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:RMSCallouts/services/apexrest/RMSService');
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        System.debug(res.getBody()); */
        
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://ap1.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9G9pzCUSkzZtdVc6yUgmYuw4XJG7tEdDlsHdu8uSSnH0dKDDeC6ch8JB3WPSTuoH.FlBKi5otggeikc2i&client_secret=9A034EA3EB3B5E9EB2DC4648BA4C4FB99AF9B1F7F5DDA3F3C1C16978869EBE3A&username=admin@rms.com&password=cskfpl@123pfc2fVzs0slJS9XUDPgDRyODU');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        HttpResponse response = h.send(request);
        System.debug('response '+response.getBody());
        Map<String, Object> results = (Map<String, Object>) Json.deserializeUntyped(response.getBody());
        string access_token =String.valueOf(results.get('access_token')); 
        Blob headerValue = Blob.valueOf(access_token);
        h = new Http();
        request = new HttpRequest();
        request.setEndpoint('https://ap1.salesforce.com/services/apexrest/RMSService');
        request.setMethod('POST');
        request.setHeader('Authorization','Bearer '+String.valueOf(results.get('access_token')));
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        string body='{"zipfile":"'+zipfile+'"}';
        request.setBody(body);
        response=new HttpResponse();
        response = h.send(request);
        System.debug('response '+response.getBody());
        
        
    }
    public PageReference deployZip(){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Deploying...'));
        MetadataService.MetadataPort service = createService();
		MetadataService.DeployOptions deployOptions = new MetadataService.DeployOptions();
        deployOptions.allowMissingFiles = false;
        deployOptions.autoUpdatePackage = false;
        deployOptions.checkOnly = false;
        deployOptions.ignoreWarnings = false;
        deployOptions.performRetrieve = false;
        deployOptions.purgeOnDelete = false;
        deployOptions.rollbackOnError = true;
        deployOptions.testLevel = 'NoTestRun';
        deployOptions.singlePackage = true;	
        system.debug('$$$Deploy');
		AsyncResult = service.deploy(MetaDataZip, DeployOptions);
        system.debug('$$$Deploy1');
        return null;
    }
    
    private static MetadataService.MetadataPort createService()
    { 
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        return service;     
    }   
    
    /**
     * Simple container class for retrieve metadata file, may as well leverage the Metadata API class for this
     **/
    public class MetadataFile extends MetadataService.MetadataWithContent
    {
        public String getFullname()
        {
            return fullName;
        }
        
        public String getContent()
        {
            return content;
        }
    }
}