@RestResource(urlMapping='/Rest_Account/*')
global class Test_Rest {

    @HttpGet
    global static List<Case> getOpenCases()
    {
        String compnayName = RestContext.request.params.get('CompanyName');
        string caseStatus = RestContext.request.params.get('status');
        Account acc =[select id, name from Account where name = :compnayName];
        List<case> ca=[select id,subject,ownerId,Owner.Name,Account.Name from case where AccountId=: acc.id and status=:caseStatus];
        
        return ca;
    }
	@httpPost
    global static string putAccount() 
    {
        system.debug('$$$json:'+RestContext.request.requestBody);
        List<testObject__c> tstObj=(List<testObject__c>)JSON.deserialize(RestContext.request.requestBody.tostring(), List<testObject__c>.class);
        insert tstObj;
        return 'Inserted successfully';
    }
    
}