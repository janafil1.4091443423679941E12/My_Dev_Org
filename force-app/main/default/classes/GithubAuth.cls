/*
  Author : Rahul Malhotra
  Description : Custom plugin for OAuth Authentication with Github
  Website:- https://sfdcstop.blogspot.com
  YouTube:- https://www.youtube.com/c/sfdcstop
  Find me on:- @rahulcoder (Twitter), @imrahulmalhotra (Instagram), https://www.facebook.com/sfdcstop
*/
global class GithubAuth extends Auth.AuthProviderPluginClass {

    public String redirectURL;
    private String key;
    private String secret;
    private String authUrl;
    private String accessTokenUrl;
    private String userInfoUrl;
    private String scope;
    
    

    // This method is responsible for returning the custom metadata storing the api credentials and other details
    global String getCustomMetadataType() {
        return 'GithubCredential__mdt';
    }

    // This method is responsible to initiate the authorization code flow
    global PageReference initiate(Map<String, String> authProviderConfiguration, String stateToPropagate) {
        // http(s)://github.com/login/oauth/authorize
        key = authProviderConfiguration.get('Consumer_Key__c');
        authUrl = authProviderConfiguration.get('Auth_URL__c');
        scope = authProviderConfiguration.get('Scope__c');
        redirectURL = authProviderConfiguration.get('CallBack_URL__c');
        String url = authUrl + '?scope='+scope+'&client_id='+key+'&redirect_uri='+redirectURL+'&state='+stateToPropagate;
        system.debug('$$$Initiate_Url'+ url);
        return new PageReference(url);
    }

    /*   
      This method is responsible to handle the callback from authorization code flow
      set the access token, refresh token and other parameters
    */
    global Auth.AuthProviderTokenResponse handleCallback(Map<String, String> authProviderConfiguration, Auth.AuthProviderCallbackState state) {

        system.debug('$$$HandleCalback_authProConfig'+ authProviderConfiguration);
       key = authProviderConfiguration.get('Consumer_Key__c');
       secret = authProviderConfiguration.get('Consumer_Secret__c');
       scope = authProviderConfiguration.get('Scope__c');
       redirectURL = authProviderConfiguration.get('CallBack_URL__c');
       accessTokenUrl = authProviderConfiguration.get('Token_URL__c');
       
        
       Map<String,String> queryParams = state.queryParameters;
        system.debug('$$$HandleCalback_queryParams'+ queryParams);
       String code = queryParams.get('code');
       String sfdcState = queryParams.get('state');
       
       HttpRequest req = new HttpRequest();
       req.setEndpoint(accessTokenURL);
       req.setHeader('Accept','application/json');
       req.setMethod('POST');
       req.setBody('client_id='+key+'&client_secret='+secret+'&code='+code+'&redirect_uri='+redirectURL+'&state='+sfdcState);

       Http http = new Http();
       HTTPResponse res = http.send(req);
       String responseBody = res.getBody();

       GithubResponse response = (GithubResponse) JSON.deserialize(responseBody, GithubResponse.class);
       system.debug('$$$HandleCalback_response'+ response);
       return new Auth.AuthProviderTokenResponse('GithubAuth', response.access_token, null, sfdcState);
    }

    // This method is responsible to get the user information used for authentication from the external api
    global Auth.UserData getUserInfo(Map<String, String> authProviderConfiguration, Auth.AuthProviderTokenResponse response) {
system.debug('$$$getUserInfo_Called');
      userInfoUrl = authProviderConfiguration.get('User_Info_URL__c');
        if(userInfoUrl != null){
            String token = response.oauthToken;

      HttpRequest req = new HttpRequest();
  		req.setHeader('Authorization', 'Bearer ' + token);
      req.setEndpoint(userInfoUrl);
      req.setMethod('GET');

      Http http = new Http();
      HTTPResponse res = http.send(req);
      String responseBody = res.getBody();

      GithubUserInfoResponse infoApiResponse = (GithubUserInfoResponse) JSON.deserialize(responseBody, GithubUserInfoResponse.class);

      String[] names = infoApiResponse.name.split(' ');
      
      String userId, fullName, firstName, lastName, locale, loginURL;

      locale = 'en-au';
      loginURL = authProviderConfiguration.get('Login_URL__c');

      userId = infoApiResponse.login;
      fullName = infoApiResponse.name;
      if(names.size()>=2) {
  			firstName = names[0];
  			lastName = names[1];
      }

      return new Auth.UserData(
        userId,
        firstName,
        lastName,
        fullName,
        null,
        null, 
        userId,
        locale,
        'Github',
        loginURL,
        null
      );
            
        }
      
        return new Auth.UserData(
        null,
        null,
        null,
        null,
        null,
        null, 
        null,
        null,
        'Github',
        null,
        null
      );
    }

    // Wrapper class to handle User Info API response
    public class GithubUserInfoResponse {
      public String login;
      public String name;
    }

    // Wrapper class to handle Access Token API response
    public class GithubResponse {
      public String access_token;
      public String token_type;
      public String scope;
    }

}