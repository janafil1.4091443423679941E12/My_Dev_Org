public class MassEmail_Controller
{
    public List<wEmailTemplate> tmpls {get;set;}
    public ApexPages.StandardSetController lstTemplates{
        get{
            if(lstTemplates ==null)
            {
            	lstTemplates =new ApexPages.StandardSetController(Database.getQueryLocator([select id from account]));
        		lstTemplates.setPageSize(5);                   
            }
            return lstTemplates; 
        }
        set;
    }
    
    public MassEmail_Controller()
    { }
    public List<wEmailTemplate> getTemplates()
    {
        tmpls = new List<wEmailTemplate>();
        wEmailTemplate wet= null;
        for(EmailTemplate et:(List<EmailTemplate>)lstTemplates.getRecords())
        {
            wet = new wEmailTemplate();
            wet.EmailTemplate = et;
            wet.isSelected = false;
            
            tmpls.add(wet);
        }
        return tmpls;
    }
    public PageReference Save()
    {    
       
        return null;
        
    }
    public Boolean hasNext{
        get{
          	return lstTemplates.getHasNext();
        }
        set;
    }
    public Boolean hasPrevious{
        get{
          	return lstTemplates.getHasPrevious();
        }
        set;
    }
    public Integer pageNumber
    {
        get{
            return lstTemplates.getPageNumber();
        }
        set;
    }
    public void first()
    {
        lstTemplates.first();
    }
    public void last()
    {
        lstTemplates.last();
    } 
    public void previous()
    {
        lstTemplates.previous();
    } 
    public void next()
    {
        lstTemplates.next();
    } 
    public class wEmailTemplate
    {
        public EmailTemplate emailTemplate{get;set;}
        public Boolean isSelected {get;set;}
    }

}