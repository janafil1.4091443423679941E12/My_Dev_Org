public with sharing class GetAllAccounts{
    @AuraEnabled (cacheable = true)
    public static List<Account> searchAccount(String accName) {
        string strAccName = '%'+ accName + '%';
        return [Select Id, Name, Website, Industry, Phone from Account WHERE Name LIKE: strAccName ];
    }

}