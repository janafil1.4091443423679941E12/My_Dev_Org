public class WSsoapClass {
    public double number1 {get;set;}
    public double number2 {get;set;}
    public double result {get;set;}
    
    public void addNumbers(){
        
        calculatorServices1.CalculatorImplPort cal =new calculatorServices1.CalculatorImplPort();
        result=cal.doAdd(number1,number2);
         
        system.debug('$$$Values:'+ number1 + '--'+ number2);
        system.debug('Result:'+ result);        
        
    }
}