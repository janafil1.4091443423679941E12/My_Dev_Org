public class ContentVersionController {
    
    public static void createContent(){
        List<ContentVersion> newList = new List<ContentVersion>();
        List<ContentVersion> cvLst = [SELECT Id, Description, VersionData, PathOnClient, Title FROM ContentVersion];
        
        for(ContentVersion cv: cvLst){
            ContentVersion newCV = new ContentVersion();
            newCV.Description = cv.Description;
            newCV.VersionData = cv.VersionData;
            newCV.PathOnClient = cv.PathOnClient;
            newCV.Title = cv.Title;
            newList.add(newCV);
        }
        
        if(newList != null && !newList.isEmpty()){
            insert newList;
            system.debug('$$$newLIST'+ newList);
            List<ContentVersion> newCvLst = [SELECT Id, Description, VersionData, PathOnClient, Title, ContentDocument.Id  FROM ContentVersion where Id=:newList];
            List<ContentDocumentLink> cdlLst = new List<ContentDocumentLink>();
            for(ContentVersion cv: newCvLst){
                ContentDocumentLink cdl= new ContentDocumentLink();
            	cdl.LinkedEntityId = '5002v000030blHaAAI';
                cdl.ContentDocumentId = cv.ContentDocument.Id;
                cdl.ShareType = 'I';
                cdlLst.add(cdl);
            }      
            insert cdlLst;
            system.debug('$$$cdlLst'+ cdlLst);
            
            
        }
        
    }

}