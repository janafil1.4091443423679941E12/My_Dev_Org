public without sharing class OrgLiensesManagementHandler {
    public OrgLiensesManagementHandler() {

    }
    @AuraEnabled(cacheable=true)  
    public static List<UserLicense> getOrgLicenses(){
        List<UserLicense> lstUserLicenses = [SELECT Id, Name, UsedLicenses, TotalLicenses, Status, MasterLabel, LicenseDefinitionKey
        FROM UserLicense ];

        return lstUserLicenses;
    }


}