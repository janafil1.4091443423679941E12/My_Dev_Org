public class PickListController {
    private static final Integer METADATA_API_VERSION = 
        Integer.valueOf(new MetadataService.MetadataPort().endpoint_x.substringAfterLast('/'));
    
    public static List<SelectOption> MetaDataTypes {get; set;}
    public static String MetaDataType {get; set;}      
    public static List<SelectOption> MetaDataItems {get; set;}
    public static String MetaDataFolder {get; set;} 
    public static String MetaDataItem {get; set;}  
    public static String MetadataFileName {get; set;}
    public static String MetadataFileData {get; set;}
    public  static String MetaDataRetrieveZip { get; private set; } 
    public static List<MetadataFile> MetadataFiles { get; set; }  
    public static MetadataService.AsyncResult AsyncResult {get; private set;}
	@AuraEnabled        
    public static List<String> getPickListValuesIntoList(){//(String objectType, String selectedField){
        List<String> pickListValuesList = new List<String>();
      /*  system.debug('$$$TEst '+ objectType);
        objectType = 'Account';
        selectedField ='AccountSource';
        Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult res = convertToObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(selectedField).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
*/
       // return pickListValuesList;
        
         // List available Metadata Types via the 'describeMetadata' API call
        MetadataService.MetadataPort service = createService();                 
        MetadataService.DescribeMetadataResult describeResult = service.describeMetadata(METADATA_API_VERSION);
        List<String> metadataTypeNames = new List<String>();
        for(MetadataService.DescribeMetadataObject metadataObject : describeResult.metadataObjects)
        {
            metadataTypeNames.add(metadataObject.xmlName);
            // Include child Metadata Types (such as CustomField, ValidationRule etc..)
            if(metadataObject.childXmlNames!=null)
                for(String childXmlName : metadataObject.childXmlNames)
                    if(childXmlName!=null)
                        metadataTypeNames.add(childXmlName);
        }   

        // Sort Metadata Types
        metadataTypeNames.sort();
        MetaDataTypes = new List<SelectOption>();               
        for(String metadataTypeName : metadataTypeNames)
            MetaDataTypes.add(new SelectOption(metadataTypeName, metadataTypeName));
                    
        // Default to first Metadata Type returned  
        MetaDataType = MetaDataTypes[0].getValue();             
        // Retrieve Metadata items for the selected Metadata Type
        //listMetadataItems();     
        system.debug('$$$metadataTypeNames:'+ metadataTypeNames);   
        return metadataTypeNames;    
    }
    
    private static MetadataService.MetadataPort createService()
	{ 
		MetadataService.MetadataPort service = new MetadataService.MetadataPort();
		service.SessionHeader = new MetadataService.SessionHeader_element();
		service.SessionHeader.sessionId =UserInfo.getSessionId(); //fetchUserSessionId();
		return service;		
	}
     public PageReference retrieveMetadataItem()
    {       
        
        // Reset state from any previous requests
        MetaDataRetrieveZip = null;
        MetadataFiles = null;
         MetaDataType = 'Profile';
        // Construct unmanaged package with list of desired components to retrieve in zip
        MetadataService.MetadataPort service = createService();             
        MetadataService.RetrieveRequest retrieveRequest = new MetadataService.RetrieveRequest();
        retrieveRequest.apiVersion = METADATA_API_VERSION;
        retrieveRequest.packageNames = null;
        retrieveRequest.singlePackage = true;
        retrieveRequest.specificFiles = null;
        retrieveRequest.unpackaged = new MetadataService.Package_x();
        retrieveRequest.unpackaged.types = new List<MetadataService.PackageTypeMembers>();
        MetadataService.PackageTypeMembers packageType = new MetadataService.PackageTypeMembers();
        system.debug('$$$MetaDataType:'+ MetaDataType);
        packageType.name = MetaDataType; 
        packageType.members = new String[] { MetadataFolder, MetaDataItem };
        retrieveRequest.unpackaged.types.add(packageType);
        AsyncResult = service.retrieve(retrieveRequest);
        system.debug('$$$$End');                
          return null;
    }
    public PageReference checkAsyncRequest(){
        MetadataService.MetadataPort service = createService();
        MetadataService.RetrieveResult retrieveResult = service.checkRetrieveStatus(AsyncResult.Id, true);
        
        if(retrieveResult.done){
            if(retrieveResult.status != 'Succeeded')
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, retrieveResult.errorMessage));
                AsyncResult = null;             
            }
            else
            {
                // Place Base64 encoded zip data onto the page for the JSZip library to handle
                MetaDataRetrieveZip = retrieveResult.zipFile;
                system.debug('$$$Callingout');
                deployZipfile(MetaDataRetrieveZip);
                MetadataFiles = new List<MetadataFile>();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Expanding...'));
                AsyncResult = null;
            }
        }
        else{
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Retrieving metadata...'));    
        }
        
        return null;
    }
    public static void deployZipfile(string zipfile){
       /*HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:RMSCallouts/services/apexrest/RMSService');
        req.setMethod('GET');
        Http http = new Http();
        HTTPResponse res = http.send(req);
        System.debug(res.getBody()); */
        
        Http h = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://ap1.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9G9pzCUSkzZtdVc6yUgmYuw4XJG7tEdDlsHdu8uSSnH0dKDDeC6ch8JB3WPSTuoH.FlBKi5otggeikc2i&client_secret=9A034EA3EB3B5E9EB2DC4648BA4C4FB99AF9B1F7F5DDA3F3C1C16978869EBE3A&username=admin@rms.com&password=cskfpl@123pfc2fVzs0slJS9XUDPgDRyODU');
        request.setMethod('GET');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        HttpResponse response = h.send(request);
        System.debug('response '+response.getBody());
        Map<String, Object> results = (Map<String, Object>) Json.deserializeUntyped(response.getBody());
        string access_token = (string)results.get('access_token'); 
       
        h = new Http();
        request = new HttpRequest();
        request.setEndpoint('https://ap1.salesforce.com/services/apexrest/RMSService');
        request.setMethod('POST');
        request.setHeader('Authorization','Bearer'+access_token);
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        string body='{zipfile:'+zipfile+'}';
        request.setBody(body);
        response=new HttpResponse();
        response = h.send(request);
        System.debug('response '+response.getBody());
        
        
    }
    private static String fetchUserSessionId(){
        String sessionId = '';
        // Refer to the Page
        PageReference reportPage = Page.GetSessionIdVF;
        // Get the content of the VF page
        String vfContent = reportPage.getContent().toString();
        System.debug('vfContent '+vfContent);
        // Find the position of Start_Of_Session_Id and End_Of_Session_Id
        Integer startP = vfContent.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
            endP = vfContent.indexOf('End_Of_Session_Id');
        // Get the Session Id
        sessionId = vfContent.substring(startP, endP);
        System.debug('sessionId '+sessionId);
        // Return Session Id
        return sessionId;
    }
    @AuraEnabled 
    public List<Profile> getPermissions(){
        List<Profile> lstProfile =[select Id,Name from Profile];        
        
        return lstProfile;
    }
    public class MetadataFile extends MetadataService.MetadataWithContent
    {
        public String getFullname()
        {
            return fullName;
        }
        
        public String getContent()
        {
            return content;
        }
    }
    
    
}