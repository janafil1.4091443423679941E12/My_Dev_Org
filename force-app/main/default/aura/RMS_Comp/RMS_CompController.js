({
    doInit : function(component) {
        var action = component.get("c.getPickListValuesIntoList");
       /* action.setParams({
            objectType: component.get("v.sObjectName"),
            selectedField: component.get("v.fieldName")
        }); */
        action.setCallback(this, function(response) {
            var list = response.getReturnValue();
            alert(list);
            component.set("v.picklistValues", list);
        })
        $A.enqueueAction(action);
    },
    onChange: function(component,event,helper){
        var action = component.get("c.getPickListValuesIntoList");
        action.setParams({
            metatype: component.find("CompType").get("v.value"),
            selectedField: component.get("v.fieldName")
        });
    }
})